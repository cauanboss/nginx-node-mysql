const express = require('express');
const app = express();
const port = 3000;
const config = {
    host: 'db',
    user: 'root',
    password: 'root',
    database: 'nodedb'
}
const mysql = require('mysql');

app.get('/', async (req,res) => {
    const connect = mysql.createConnection(config);    
    try {
        await createTable(connect);
        const retInsert = await insertName(connect);
        const retSelect = await getName(connect); 
        console.log(retSelect);
        let listNames = '';
        connect.end();
        retSelect.map((v,i)=>{
            listNames += `<label>${retSelect[i].name}</label></br>`;
        });
        let html = `
        <h1>Full Cycle Rocks!</h1>
        </br>
        <h2>Lista de nomes</h2>
        </br>
        <div>
            ${listNames}
        </div>
        `;
        res.send(html);    
    } catch (error) {
        connect.end();
        res.send(`<h1>${JSON.stringify(error)}</h1>`);
    }
    
});

function insertName(connect){
    return new Promise(function (resolve, reject) {
        const sql = `INSERT INTO people (name) values('Cauan')`;
        connect.query(sql, (error, results, fields) => {
            if (error) {
                return reject(error);
            }
            resolve(results);
        });
    });
}

function getName(connect) {
    return new Promise(function (resolve, reject) {
        const sql = `SELECT id, name FROM people;`;
        connect.query(sql, (error, results, fields) => {
            if (error) {
                return reject(error);
            }
            resolve(results);
          });
    });
}

function createTable(connect) {
    return new Promise(function (resolve, reject) {
        const sql = `
        CREATE TABLE IF NOT EXISTS nodedb.people (
            id int not null auto_increment,
            name varchar(255) not null,
            primary key(id)
        )`;
        connect.query(sql, (error, results, fields) => {
            if (error) {
                return reject(error);
            }
            resolve(results);
          });
    });
}





app.listen(port, ()=>{
    console.log('Rodando');
});